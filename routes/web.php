<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home')->name('login');
Route::get('dashboard', 'PagesController@dashboard')->name('dashboard');

// Authentication
Route::post('loginRegister', 'AuthController@loginRegister')->name('loginRegister');
Route::post('logout', 'AuthController@logout')->name('logout');
Route::get('activate-user/{activationCode}', 'AuthController@activate')->name('activate');





