<?php

namespace Tests\Feature;

use App\Mail\ActivationEmail;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * @test
     */
    public function guest_can_visit_homepage()
    {

        $this->get('/')
            ->assertStatus(200)
            ->assertSee('Login / Register');
    }


    /** @test */
    public function authenticated_user_cannot_visit_homepage_to_login_again()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->get(route('login'))
            ->assertDontSee('Login / Register')
            ->assertRedirect(route('dashboard'));

    }


    /** @test */
    public function authenticated_user_can_logout()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->assertAuthenticated()
            ->post(route('logout'))
            ->assertRedirect(route('login'));

        $this->assertGuest();


    }

    /** @test */
    public function guest_cannot_logout()
    {

        $this->assertGuest()->post(route('logout'))
            ->assertRedirect(route('login'));

    }

    /** @test */
    public function guest_can_register_if_email_does_not_exists()
    {

        Mail::fake();

        $this->assertEquals(0, User::count());

        $this->assertGuest()
            ->post(route('loginRegister'),
                [
                    'email' => 'email@email.com',
                    'password' => 'password'
                ])
            ->assertRedirect(route('dashboard'));

        Mail::assertQueued(ActivationEmail::class);

        $this->assertAuthenticated();

        $this->assertEquals(1, User::count());


    }


    /** @test */
    public function guest_can_simply_login_if_already_registered()
    {

        Mail::fake();

        $user = factory(User::class)->create();

        $this->assertEquals(1, User::count());

        $this->post(route('loginRegister'),
            [
                'email' => $user->email,
                'password' => 'secret'
            ])
            ->assertRedirect(route('dashboard'));

        $this->assertEquals(1, User::count());

        Mail::assertNotQueued(ActivationEmail::class);

        $this->assertAuthenticated();

    }

    /** @test */
    public function guest_cannot_login_with_incorrect_credentials()
    {
        $user = factory(User::class)->create();

        $this->assertGuest();

        $this->post(route('loginRegister'), [
            'email' => $user->email,
            'password' => 'incorrectpassword'
            ])
            ->assertRedirect(route('login'))
            ->assertSessionHasErrors('Invalid credentials');

        $this->assertGuest();

    }

    /** @test */
    public function guest_cannot_register_with_invalid_email_format()
    {
        $this->assertGuest();

        $this->post(route('loginRegister'), [
            'email' => 'notemail',
            'password' => 'incorrectpassword'
        ])
            ->assertRedirect(route('login'))
            ->assertSessionHasErrors('email');

        $this->assertGuest();

    }

    /** @test */
    public function guest_cannot_register_with_password_shorter_than_5_characters()
    {
        $this->assertGuest();

        $this->post(route('loginRegister'), [
            'email' => 'a@a.com',
            'password' => '1234'
        ])
            ->assertRedirect(route('login'))
            ->assertSessionHasErrors('password');

        $this->assertGuest();

    }
    
    /** @test */
    public function user_can_activate_account_with_activation_link()
    {

        $user = factory(User::class)->create([
            'isActivated' => false
        ]);

        $this->assertEquals(0, User::first()->isActivated);

        $this->actingAs($user)
            ->get(route('activate', ['activationCode' => $user->activationCode]))
            ->assertRedirect(route('dashboard'));

        $this->assertEquals(1, User::first()->isActivated);

    }
    
    /** @test */
    public function user_cannot_activate_account_with_incorrect_activation_link()
    {

        $user = factory(User::class)->create([
            'isActivated' => false
        ]);

        $this->assertEquals(0, User::first()->isActivated);
        $this->assertNotNull(User::first()->activationCode);

        $this->actingAs($user)
            ->get(route('activate', ['activationCode' => str_random(50)]))
            ->assertViewIs('pages.activation-failed');

        $this->assertEquals(0, User::first()->isActivated);
        $this->assertNotNull(User::first()->activationCode);


    }


}
