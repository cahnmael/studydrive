<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActivationEmail extends Mailable
{
    use Queueable, SerializesModels;


    public $user;

    /**
     * Create a new activation message instance.
     *
     * @param User $user The email recipient
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the activation message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.activation');
    }
}
