<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password','isActivated', 'activationCode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *
     * Register a user and saves into the database
     *
     *
     * @param $credentials
     * @param $activationCode
     * @return User object
     */
    public static function register($credentials, $activationCode)
    {
        return static::create([
            'email' => $credentials['email'],
            'password' => bcrypt($credentials['password']),
            'isActivated' => false,
            'activationCode' => $activationCode
        ]);
    }

    /**
     * Activates a user account and removes used activation code.
     */
    public function activate()
    {
        $this->isActivated = true;
        $this->activationCode = null; // remove unique key
        $this->save();
    }

}
