<?php

namespace App\Http\Controllers;


class PagesController extends Controller
{

    /**
     *
     * Assigns middleware to pages.
     *
     * PagesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->only('dashboard'); // Only logged in users can land on the dashboard page

        $this->middleware('guest')->only('home'); // Logged in users should not be shown login form again
    }


    /**
     *
     * Serves the homepage with login/register form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        return view('pages.home');
    }


    /**
     *
     * Serves the dashboard page where authenticated users land after successful
     * registration or login process
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        return view('pages.dashboard');
    }

}
