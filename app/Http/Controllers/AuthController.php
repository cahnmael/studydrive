<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRegisterRequest;
use App\Mail\ActivationEmail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PhpParser\Node\Scalar\String_;

class AuthController extends Controller
{


    /**
     * AuthController constructor.
     *
     * Defines the middlewares for this controller
     *
     */
    public function __construct()
    {

        $this->middleware('guest')->only('loginRegister'); // only guest can login or register
        $this->middleware('auth')->only('logout'); // only logged in user can logout

    }


    /**
     *
     * Decides if incoming user should be registered or logged in.
     *
     * Registers a user if the email does not exists in database. Otherwise, logs the user in.
     *
     * @param LoginRegisterRequest $request
     * @return AuthController|\Illuminate\Http\RedirectResponse
     */
    public function loginRegister(LoginRegisterRequest $request)
    {

        // Check if user with this email already exists, if yes, authenticate the user, if not register this user
        if(User::whereEmail($request->input('email'))->exists()){

            return $this->logUserIn($request->only(['email', 'password']));

        }

        return $this->registerUser($request->only(['email', 'password']));

    }

    /**
     *
     * Logs an authenticated user out.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {

        auth()->logout();

        return redirect()->route('login');
    }


    /**
     *
     * Activate a user's account after a correct activation code is given
     *
     * @param $activationCode
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function activate($activationCode)
    {

        // check if activation code matches
        $user = User::where('activationCode', $activationCode)->first();

        if($user){

            $user->activate();

            session()->flash('flash-message', 'Your account has been successfully activated');

            // For security, if users is not logged in, send back to home page along with flash message.
            if(! auth()->check()){
                return redirect()->route('login');
            }

            return redirect()->route('dashboard');

        }

        return view('pages.activation-failed');

    }


    /**
     * Attempt to log a user in.
     *
     * Redirect to dashboard if authentication successful.
     *
     * Redirect back to login page is authentication fails
     *
     * @param $credentials
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    private function logUserIn($credentials)
    {
        if(auth()->attempt($credentials)){

            return redirect()->intended(route('dashboard'));

        }else{

            return redirect()->route('login')
                ->withInput()
                ->withErrors(['Invalid credentials' => 'The credentials you have entered are invalid']);

        }
    }

    /**
     *
     * Create random and unique user's activation code
     *
     * @return string
     */
    private function generateUniqueRandomString()
    {

        // check if activation code with that string already exists before applying
        do{

            $activationCode = str_random(50);

        }while(User::where('activationCode',$activationCode)->exists());

        return $activationCode;

    }

    /**
     *
     * Performs registration process.
     *
     * 1. Creates a user with unique activation code
     * 2. Sends activation Email
     * 3. Log the user in with a success message
     *
     * @param array $credentials
     * @return \Illuminate\Http\RedirectResponse
     */
    private function registerUser(array $credentials)
    {

        $user = User::register(
            $credentials,
            $this->generateUniqueRandomString()
        );

        // Sends email using queues so that users don't have to wait
        // until email is sent before receiving response
        Mail::to($user->email)->queue(new ActivationEmail($user));

        auth()->login($user);

        session()->flash('flash-message' ,
            'Your Registration is successful. Please check your email for account activation link'
        );

        return redirect()->route('dashboard');
    }



}
