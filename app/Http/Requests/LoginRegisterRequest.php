<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /*
         * Validating email uniqueness is not done here since it needs to be manually checked
         * to determine whether we should perform login or registration.
         *
         * If it is done here, then registered user will not go through the login process since the
         * his/her email is already in database and triggers 'unique' validation error.
         *
         */

        return [
            'email' => 'required|email',
            'password' => 'required|min:5'
        ];
    }
}
