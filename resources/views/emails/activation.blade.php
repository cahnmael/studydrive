{{-- Email containing link to activate the user's account --}}
@component('mail::message')
# Thanks for registering

Click the button below to activate your account

@component('mail::button', ['url' => route('activate', ['activationCode' => $user->activationCode])])
Activate Your Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
