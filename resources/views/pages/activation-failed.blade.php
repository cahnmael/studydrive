{{-- Shows the page when the  --}}
@extends('layout.main')

@section('title', 'Account Activation Failed')

@section('content')

    @include('partials.nav')

    <h1>Account Activation Failed</h1>
    <p>
        Your account activation failed. Please use a valid confirmation link.
    </p>

@endsection