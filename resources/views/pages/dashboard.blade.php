{{-- Show the dashboard page where users will land on after successful registration or login process --}}
@extends('layout.main')

@section('title', 'Dashboard')

@section('content')

    @include('partials.nav')

    <div class="row">
        <div class="col-12">
            @include('partials.alert')
        </div>
    </div>


    <div class="row justify-content-center mt-4">
        <div class="col-4">
            {{-- Shows details of the current authenticated user --}}
            <div class="card">
                <div class="card-body">
                    <p><b>User ID:</b> {{ auth()->id() }}</p>
                    <p><b>Email Address:</b> {{ auth()->user()->email }}</p>
                    <p><b>Activation Status:</b>
                        @if(auth()->user()->isActivated)
                            <span class="badge badge-success">Activated</span>
                        @else
                            <span class="badge badge-secondary">Not Activated</span>
                        @endif
                    </p>
                </div>
            </div>

        </div>
    </div>

@endsection