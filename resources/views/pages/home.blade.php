<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link href="{{ asset('css/signin.css') }}" rel="stylesheet">


    <title>Studydrive - Home</title>
</head>
<body class="text-center">

<div class="container">

    <div class="row justify-content-md-center">
        <div class="col-md-6">
            @include('partials.alert')
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            {{-- Register / Login Form --}}
            <form class="form-signin" method="post" action="{{ route('loginRegister') }}">
                <h1 class="h3 mb-3 font-weight-normal">Login / Register</h1>

                <label for="email" class="sr-only">Email address</label>
                <input name="email" type="email" id="email" class="form-control" placeholder="Email address" required autofocus value="{{ old('email') }}">
                <label for="password" class="sr-only">Password</label>
                <input name="password" type="password" id="password" class="form-control" placeholder="Password" required>
                {{ csrf_field() }}
                <button class="btn btn-lg btn-primary btn-block" type="submit">Login / Register</button>
                <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
            </form>
        </div>
    </div>




</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>
</html>